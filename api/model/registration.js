const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    name:{
        type:String,
        trim:true,
        require:true
    },
    email:{
        type:String,
        trim:true,
        unique:true,
    },
    phone:{
        type:Number,
    },
    password:{
        type:String,
    }

});

module.exports = mongoose.model('user', UserSchema);