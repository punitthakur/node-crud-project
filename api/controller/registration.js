var registerModel = require("../model/registration");

class Register{
    constructor(){

    }
    
    register(req,res){
        console.log("the data in the body is:",req.body);
      registerModel.findOne({email:req.body.email}).then((doc)=>{
       if(!doc){
        console.log("doc ============== ",doc);
           let registerObj = new registerModel();
           registerObj.name = req.body.name
           registerObj.email = req.body.email
           registerObj.phone = req.body.phone
           registerObj.password = req.body.password
           registerObj.save().then((saved)=>{
               console.log("saved ----------- ",saved);
               if(saved){
                  return res.json({status:true, message:"Signup successful.", data:saved});
               }else{
                 return  res.json({status:false});
               }
           })
       }
      }).catch((err)=>{
          console.log(err)
      })
    };
}

module.exports = new Register();