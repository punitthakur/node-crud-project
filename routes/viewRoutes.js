var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('registration');
});

/*get the registration page */
router.get('/login', function(req, res, next) {
  res.render('login');
});

module.exports = router;
