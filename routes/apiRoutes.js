var express = require('express');
var router = express.Router();
const registerController = require('../api/controller/registration');
const loginController = require('../api/controller/login');


/* GET users listing. */
router.post('/register',registerController.register);

router.post('/login',loginController.login);

module.exports = router;
